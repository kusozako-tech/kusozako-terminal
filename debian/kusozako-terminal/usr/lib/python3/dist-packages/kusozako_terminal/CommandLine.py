
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


OPTIONS = [
    (
        "working-directory",
        ord("d"),
        GLib.OptionFlags.NONE,
        GLib.OptionArg.STRING,
        _("directory to open"),
        _("directory"),
    ),
    (
        "execute",
        ord("x"),
        GLib.OptionFlags.NONE,
        GLib.OptionArg.STRING,
        _("execute command"),
        _("command"),
    ),
    (
        "version",
        ord("V"),
        GLib.OptionFlags.NONE,
        GLib.OptionArg.NONE,
        _("show version"),
        None
    )
]
