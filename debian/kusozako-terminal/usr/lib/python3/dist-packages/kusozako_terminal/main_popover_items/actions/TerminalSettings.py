
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class AlfaTerminalSettings(DeltaEntity):

    ACTION_ID = "define acceptable action id here."

    def _action(self, param=None):
        raise NotImplementedError

    def receive_transmission(self, user_data):
        action_id, param = user_data
        if action_id == self.ACTION_ID:
            self._action(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register action object", self)
