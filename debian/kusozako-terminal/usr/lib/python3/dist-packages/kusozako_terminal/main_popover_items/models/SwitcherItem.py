
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

SWITCHER_ITEM = {
    "type": "switcher",
    "title": _("Terminal"),
    "message": "delta > switch stack to",
    "user-data": "terminal-settings",
}
