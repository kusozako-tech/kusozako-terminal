
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .models.Models import DeltaModels
from .actions.Actions import EchoActions


class DeltaMainPopoverItems(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        DeltaModels(self)
        EchoActions(self)
