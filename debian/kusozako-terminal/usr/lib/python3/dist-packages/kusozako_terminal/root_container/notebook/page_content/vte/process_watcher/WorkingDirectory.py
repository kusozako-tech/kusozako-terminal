
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3 import HomeDirectory


class FoxtrotWorkingDirectory:

    def update(self):
        try:
            directory = GLib.file_read_link(self._path)
            return HomeDirectory.shorten(directory)
        except GLib.Error:
            return None

    def get_working_directory(self):
        try:
            directory = GLib.file_read_link(self._path)
            return directory
        except GLib.Error:
            return None

    def __init__(self, pid):
        # /proc/PID/cwd is symbolic-link
        self._path = "/proc/{}/cwd".format(pid)
