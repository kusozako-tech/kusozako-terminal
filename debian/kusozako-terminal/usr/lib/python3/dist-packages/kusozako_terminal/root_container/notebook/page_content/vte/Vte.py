
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Vte
from libkusozako3.Entity import DeltaEntity
from .Pty import DeltaPty
from .process_watcher.ProcessWatcher import DeltaProcessWatcher
from .watchers.Watchers import DeltaWatchers
from .settings.Settings import EchoSettings


class DeltaVte(Vte.Terminal, DeltaEntity):

    @classmethod
    def new_for_directory(cls, parent, directory=None):
        terminal = cls(parent)
        terminal.spawn_pty(directory)
        return terminal

    def _delta_call_spawn_finished(self, pid):
        self._process_watcher = DeltaProcessWatcher.new_for_pid(self, pid)
        self.grab_focus()

    def _delta_info_vte(self):
        return self

    def _delta_call_add_new_tab(self):
        directory = self._process_watcher.get_working_directory()
        self._raise("delta > add new tab", directory)

    def get_process_name(self):
        # terminal instance is direct child widget of notebook. so...
        # this function will be accessed from Gtk.Container.get_children()
        # on tab or allicaton closing sequence.
        return self._process_watcher.get_process_name()

    def can_close(self):
        return self._process_watcher.can_close()

    def spawn_pty(self, directory=None):
        DeltaPty.new_for_directory(self, directory)

    def _on_realize(self, vte):
        DeltaWatchers(self)

    def __init__(self, parent):
        self._parent = parent
        Vte.Terminal.__init__(self)
        self.set_scrollback_lines(-1)
        EchoSettings(self)
        self.connect("realize", self._on_realize)
