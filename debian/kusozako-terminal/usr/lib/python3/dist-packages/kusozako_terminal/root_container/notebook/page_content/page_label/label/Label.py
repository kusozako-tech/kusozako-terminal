
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from libkusozako3.Entity import DeltaEntity
from .actions.Actions import EchoActions


class DeltaLabel(Gtk.Label, DeltaEntity):

    def _delta_info_label(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            "kusozako-terminal",
            ellipsize=Pango.EllipsizeMode.MIDDLE,
            hexpand=True
            )
        self._raise("delta > add to container", self)
        EchoActions(self)
