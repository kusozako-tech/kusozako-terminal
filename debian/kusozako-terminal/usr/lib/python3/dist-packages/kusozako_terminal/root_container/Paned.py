
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_terminal import SplitDirection
from .notebook.Notebook import DeltaNotebook


class DeltaPaned(Gtk.Paned, DeltaEntity):

    @classmethod
    def new(cls, parent, notebook, orientation, direction):
        paned = cls(parent)
        paned.set_notebooks(notebook, orientation, direction)
        return paned

    def _set_position(self, notebook, orientation, rectangle):
        rectangle, _ = notebook.get_allocated_size()
        if orientation == Gtk.Orientation.HORIZONTAL:
            self.set_position(int(rectangle.width/2))
        else:
            self.set_position(int(rectangle.height/2))

    def _set_notebooks(self, notebook, direction):
        if direction == SplitDirection.START:
            self.add1(DeltaNotebook(self))
            self.add2(notebook)
        elif direction == SplitDirection.END:
            self.add1(notebook)
            self.add2(DeltaNotebook(self))

    def set_notebooks(self, notebook, orientation, direction):
        self.set_orientation(orientation)
        self._set_notebooks(notebook, direction)
        self._set_position(notebook, orientation, direction)

    def _get_method_name(self, parent_container):
        if isinstance(parent_container, Gtk.Bin):
            return "add"
        return "add1" if parent_container.get_child1() == self else "add2"

    def reparent_unremoved_child(self):
        unremoved_child = self.get_children()[0]
        parent_container = self.get_parent()
        self.remove(unremoved_child)
        method_name = self._get_method_name(parent_container)
        parent_container.remove(self)
        method = getattr(parent_container, method_name)
        method(unremoved_child)
        self.destroy()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Paned.__init__(self, wide_handle=True)
