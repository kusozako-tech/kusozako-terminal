
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SplitTerminal import SPLIT_TERMINAL

MAIN_PAGE = {
    "page-name": "main",
    "items": [
        {
            "type": "simple-action",
            "title": _("Copy"),
            "message": "delta > copy clipboard",
            "user-data": None,
            "shortcut": "Shit+Ctrl+C",
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Paste"),
            "message": "delta > paste clipboard",
            "user-data": None,
            "shortcut": "Shit+Ctrl+V",
            "close-on-clicked": True
        },
        {
            "type": "separator"
        },
        {
            "type": "switcher",
            "title": _("Split Terminal"),
            "message": "delta > switch stack to",
            "user-data": "split-terminal",
        },
        {
            "type": "separator"
        },
        {
            "type": "simple-action",
            "title": _("Add New Tab"),
            "message": "delta > add new tab",
            "user-data": None,
            "shortcut": "Shit+Ctrl+T",
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Close Tab"),
            "message": "delta > close tab",
            "user-data": None,
            "shortcut": "Shit+Ctrl+W",
            "close-on-clicked": True
        },

    ]
}


MODEL = [MAIN_PAGE, SPLIT_TERMINAL]
