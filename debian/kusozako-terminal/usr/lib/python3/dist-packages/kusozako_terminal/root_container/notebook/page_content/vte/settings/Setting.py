
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class AlfaSetting(DeltaEntity):

    KEY = "define settings key here"
    DEFAULT = "define default value here."

    def _reset_setting(self, vte, setting):
        raise NotImplementedError

    def _reset(self, setting):
        vte = self._enquiry("delta > vte")
        self._reset_setting(vte, setting)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "vte" and key == self.KEY:
            self._reset(value)

    def __init__(self, parent):
        self._parent = parent
        query = "vte", self.KEY, self.DEFAULT
        current_font = self._enquiry("delta > settings", query)
        self._reset(current_font)
        self._raise("delta > register settings object", self)
