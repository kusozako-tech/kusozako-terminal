
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .label.Label import DeltaLabel
from .Popover import DeltaPopover


class DeltaPageLabel(Gtk.EventBox, DeltaEntity):

    def _delta_info_event_source(self):
        return self

    def _delta_call_add_to_container(self, widget):
        self.add(widget)
        self.show_all()

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventBox.__init__(self)
        DeltaLabel(self)
        DeltaPopover(self)
