
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_terminal import TerminalSignals

TOOLTIP_TEMPLATE = "pid: {}\nworking directory: {}\nchild process: {}"


class DeltaTooltip(DeltaEntity):

    def _action(self, signal_param):
        label = self._enquiry("delta > label")
        tooltip = TOOLTIP_TEMPLATE.format(*signal_param)
        label.props.tooltip_text = tooltip

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal == TerminalSignals.UPDATE:
            self._action(signal_param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register terminal object", self)
