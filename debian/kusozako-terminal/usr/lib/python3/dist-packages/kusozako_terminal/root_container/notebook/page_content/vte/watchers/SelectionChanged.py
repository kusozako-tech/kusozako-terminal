
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaSelectionChanged(DeltaEntity):

    def _on_selection_changed(self, vte):
        query = "vte", "copy_on_select", False
        copy_on_select_enabled = self._enquiry("delta > settings", query)
        if copy_on_select_enabled and vte.get_has_selection():
            self._raise("delta > copy clipboard")

    def __init__(self, parent):
        self._parent = parent
        vte = self._enquiry("delta > vte")
        vte.connect("selection-changed", self._on_selection_changed)
