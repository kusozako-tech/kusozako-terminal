
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .page_label.PageLabel import DeltaPageLabel
from .vte.Vte import DeltaVte


class DeltaPageContent(DeltaEntity):

    @classmethod
    def new_for_directory(cls, parent, directory=None):
        page_content = cls(parent)
        page_content.setup(directory)

    def _delta_call_close_tab(self):
        if self._vte.can_close():
            self._raise("delta > remove page", self._vte)

    def _delta_call_terminal_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_terminal_object(self, object_):
        self._transmitter.register_listener(object_)

    def setup(self, directory=None):
        page_label = DeltaPageLabel(self)
        self._vte = DeltaVte.new_for_directory(self, directory)
        self._raise("delta > append page", (self._vte, page_label))

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
