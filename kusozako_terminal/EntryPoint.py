
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .root_container.RootContainer import DeltaRootContainer
from .main_popover_items.MainPopoverItems import DeltaMainPopoverItems


class DeltaEntryPoint(DeltaEntity):

    def _delta_info_working_directory(self):
        return self._working_directory

    def _delta_info_startup_command(self):
        command = self._command
        self._command = None
        return command

    def __init__(self, parent):
        self._parent = parent
        DeltaMainPopoverItems(self)
        self._working_directory = self._enquiry(
            "delta > command line option",
            "working-directory"
            )
        self._command = self._enquiry("delta > command line option", "execute")
        DeltaRootContainer(self)
