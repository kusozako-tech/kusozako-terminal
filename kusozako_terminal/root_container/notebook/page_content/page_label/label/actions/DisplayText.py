
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_terminal import TerminalSignals


class DeltaDisplayText(DeltaEntity):

    def _action(self, signal_param):
        label = self._enquiry("delta > label")
        pid, working_directory, child_process = signal_param
        if child_process is not None:
            label.set_text(child_process)
        elif working_directory != "~/":
            label.set_text(working_directory)
        else:
            label.set_text("PID: {}".format(pid))

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal == TerminalSignals.UPDATE:
            self._action(signal_param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register terminal object", self)
