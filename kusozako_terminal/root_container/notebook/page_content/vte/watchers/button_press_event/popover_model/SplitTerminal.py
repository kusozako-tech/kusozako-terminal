
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_terminal import SplitDirection

SPLIT_TERMINAL = {
    "page-name": "split-terminal",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "main",
        },
        {
            "type": "separator"
        },
        {
            "type": "simple-action",
            "title": _("Left"),
            "message": "delta > split",
            "user-data": (Gtk.Orientation.HORIZONTAL, SplitDirection.START),
            "shortcut": "Shift+Ctrl+H",
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Right"),
            "message": "delta > split",
            "user-data": (Gtk.Orientation.HORIZONTAL, SplitDirection.END),
            "shortcut": "Shift+Ctrl+J",
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Top"),
            "message": "delta > split",
            "user-data": (Gtk.Orientation.VERTICAL, SplitDirection.START),
            "shortcut": "Shift+Ctrl+K",
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Bottom"),
            "message": "delta > split",
            "user-data": (Gtk.Orientation.VERTICAL, SplitDirection.END),
            "shortcut": "Shift+Ctrl+L",
            "close-on-clicked": True
        },
    ]
}
