
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_terminal import SplitDirection

MAIN_PAGE = {
    "page-name": "main",
    "items": [
        {
            "type": "simple-action",
            "title": _("Copy"),
            "message": "delta > copy clipboard",
            "user-data": None,
            "shortcut": "Shit+Ctrl+C",
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Paste"),
            "message": "delta > paste clipboard",
            "user-data": None,
            "shortcut": "Shit+Ctrl+V",
            "close-on-clicked": True
        },
        {
            "type": "separator"
        },
        {
            "type": "label",
            "title": _("Split Terminal :")
        },
        {
            "type": "simple-action",
            "title": _("Left"),
            "message": "delta > split",
            "user-data": (Gtk.Orientation.HORIZONTAL, SplitDirection.START),
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Right"),
            "message": "delta > split",
            "user-data": (Gtk.Orientation.HORIZONTAL, SplitDirection.END),
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Top"),
            "message": "delta > split",
            "user-data": (Gtk.Orientation.VERTICAL, SplitDirection.START),
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Bottom"),
            "message": "delta > split",
            "user-data": (Gtk.Orientation.VERTICAL, SplitDirection.END),
            "close-on-clicked": True
        },
        {
            "type": "separator"
        },
        {
            "type": "simple-action",
            "title": _("Add New Tab"),
            "message": "delta > add new tab",
            "user-data": None,
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Close Tab"),
            "message": "delta > close tab",
            "user-data": None,
            "close-on-clicked": True
        },

    ]
}


MODEL = [MAIN_PAGE]
