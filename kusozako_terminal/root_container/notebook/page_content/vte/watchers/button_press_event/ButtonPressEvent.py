
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu
from .popover_model.PopoverModel import MODEL


class DeltaButtonPressEvent(DeltaEntity):

    def _on_button_press(self, vte, event, popover_menu):
        if event.button != 3:
            return
        popover_menu.popup_for_position(vte, event.x, event.y)

    def __init__(self, parent):
        self._parent = parent
        vte = self._enquiry("delta > vte")
        popover_menu = DeltaPopoverMenu.new_for_model(self, MODEL)
        vte.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)
        vte.connect("button-press-event", self._on_button_press, popover_menu)
