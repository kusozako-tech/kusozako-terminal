
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from libkusozako3.Entity import DeltaEntity


class DeltaBackgroundColor(DeltaEntity):

    def _reset(self, color_to_parse):
        gdk_rgba = Gdk.RGBA()
        gdk_rgba.parse(color_to_parse)
        vte = self._enquiry("delta > vte")
        vte.set_color_background(gdk_rgba)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == "vte" and key == "background_color":
            self._reset(value)

    def __init__(self, parent):
        self._parent = parent
        gdk_rgba = Gdk.RGBA()
        gdk_rgba.parse("black")
        query = "vte", "background_color", gdk_rgba.to_string()
        current_color = self._enquiry("delta > settings", query)
        self._reset(current_color)
        self._raise("delta > register settings object", self)
