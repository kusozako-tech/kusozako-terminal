
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Vte
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity


class DeltaPty(DeltaEntity):

    @classmethod
    def new_for_directory(cls, parent, directory=None):
        pty = cls(parent)
        pty.spawn_for_directory(directory)

    def _on_spawn_finished(self, vte, pid, error):
        self._raise("delta > spawn finished", pid)
        command = self._enquiry("delta > startup command")
        if command is None:
            return
        if not command.endswith("\n"):
            command += "\n"
        vte.feed_child(command.encode("utf-8"))

    def spawn_for_directory(self, directory=None):
        vte = self._enquiry("delta > vte")
        if directory is None:
            directory = self._enquiry("delta > working directory")
        vte.spawn_async(
            Vte.PtyFlags.DEFAULT,               # 0. pty flags
            directory,                          # 1. working directory
            ["/bin/bash"],                      # 2. argv
            None,                               # 3. eenv
            GLib.SpawnFlags.DO_NOT_REAP_CHILD,  # 4. spawn flags
            None,                               # 5. child setup func
            None,                               # 6. child setup func data
            -1,                                 # 7. timeout
            None,                               # 8. cancellable
            self._on_spawn_finished             # 9. callback
            )

    def __init__(self, parent):
        self._parent = parent
