
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


class FoxtrotProcessParser:

    def _check_running_process(self, vte):
        process = vte.get_process_name()
        if process is not None:
            self._processes.append(process)

    def _parse_container(self, container):
        for child in container.get_children():
            if isinstance(child, Gtk.Container):
                self._parse_container(child)
            else:
                self._check_running_process(child)

    def parse(self):
        self._processes.clear()
        self._parse_container(self._root_container)
        return self._processes.copy()

    def __init__(self, root_container):
        self._processes = []
        self._root_container = root_container
