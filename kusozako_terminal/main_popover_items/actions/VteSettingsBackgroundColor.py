
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.color_selection.ColorSelection import DeltaColorSelection
from .TerminalSettings import AlfaTerminalSettings


class DeltaVteSettingsBackgroundColor(AlfaTerminalSettings):

    SIGNAL = "vte.settings.background-color"

    def _action(self, param=None):
        query = "vte", "background_color", ""
        current_color = self._enquiry("delta > settings", query)
        model = {
            "title": _("Select Background Color"),
            "current-color": current_color
            }
        selected_color = DeltaColorSelection.get_color(self, model)
        if selected_color is None:
            return
        user_data = "vte", "background_color", selected_color
        self._raise("delta > settings", user_data)
