
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .TerminalSettingsTextColor import DeltaTerminalSettingsTextColor
from .TerminalSettingsFont import DeltaTerminalSettingsFont
from .VteSettingsBackgroundColor import DeltaVteSettingsBackgroundColor


class EchoActions:

    def __init__(self, parent):
        DeltaTerminalSettingsTextColor(parent)
        DeltaVteSettingsBackgroundColor(parent)
        DeltaTerminalSettingsFont(parent)
