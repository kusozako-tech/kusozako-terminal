
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

UPDATE = 0          # (int, str, str) as (pid, working_dir, process_name)
TRY_CLOSE_TAB = 1   # None
